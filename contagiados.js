// CONTAGIADOS
// Empezamos con un array:[true, false, true, false, false, false, true, true].
// Las posiciones(pacientes) con false están sanas, las que tienen true son contagiosas.
// Necesitamos una función que nos diga los contagiados el día siguiente.
// Cada enfermo contagiará a los que se encuentran justo a su lado.
// El resultado, para el array de arriba seria: [true, true, true, true, false, true, true, true]

const grupoPacientes = [true, false, true, false, false, false, true, true]

////////////////////////////////////////////////////////
// Solución 1: buscamos CONTAGIADOS(true) con un For.//
//////////////////////////////////////////////////////

const contagiadosDiaSiguiente = (grupoPacientes) =>{
    //Si el parametro recibido es null lanzamos error
    if (grupoPacientes == null) throw new Error("Grupo incorrecto 1");
    //Hacemos copia del grupo real para no alterar
    const grupoPacientesDiaSiguiente = [...grupoPacientes];

    // Recorremos el grupo(array) real para buscar a los contagiados
    for (let i = 0; i < grupoPacientes.length; i++) {
        //Comprobamos si el paciente está contagiado (true)
       if(grupoPacientes[i] === true){
           //Comprobamos si es el primer paciente ya que solo puede contagiar al de su "derecha".
            if (i === 0) grupoPacientesDiaSiguiente[i+1] = true;

            else if (grupoPacientes.includes(grupoPacientes[i+1])){
                
                //Sino es el primer paciente comprobamos que no sea el último viendo si el paciente a su "derecha" existe.
                //Al no ser el último ni el primero contagiado a su izquierda y derecha.
                grupoPacientesDiaSiguiente[i-1] = true;
                grupoPacientesDiaSiguiente[i+1] = true;

            }else grupoPacientesDiaSiguiente[i-1] = true; //Si no existe paciente a su "derecha" es que es el último por lo tanto solo contagia a su "izquierda".

        }
        
    }
    //Devuelvo el grupo(array) contagiado al siguiente día
    return grupoPacientesDiaSiguiente;


}

///////////////////////////////////////////////////////////
// Solución 2: buscamos CONTAGIADOS(true) con un ForEach.//
///////////////////////////////////////////////////////////

const contagiadosDiaSiguiente2 = (grupoPacientes) =>{
    //Si el parametro recibido es null lanzamos error
    if (grupoPacientes == null) throw new Error("Grupo incorrecto 2");
    //Hacemos copia del grupo real para no alterar
    const grupoPacientesDiaSiguiente = [...grupoPacientes];
    // Recorremos el grupo(array) real para buscar a los contagiados
    grupoPacientes.forEach((paciente, indice) => {
        //Comprobamos si el paciente está contagiado (true)
        if (paciente === true){
           //Comprobamos si es el primer paciente ya que solo puede contagiar al de su "derecha".
            if (indice === 0)grupoPacientesDiaSiguiente[indice+1] = true;

            else if (grupoPacientes.includes(grupoPacientes[indice+1])){
                
                //Sino es el primer paciente comprobamos que no sea el último viendo si el paciente a su "derecha" existe.
                //Al no ser el último ni el primero contagiado a su izquierda y derecha.
                grupoPacientesDiaSiguiente[indice-1] = true;
                grupoPacientesDiaSiguiente[indice+1] = true;

            }else grupoPacientesDiaSiguiente[indice-1] = true; //Si no existe paciente a su "derecha" es que es el último por lo tanto solo contagia a su "izquierda".


        }
    })

    //Devuelvo el grupo(array) contagiado al siguiente día
    return grupoPacientesDiaSiguiente;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Solución 3: buscamos SANOS(false) con un ForEach (La estructura con el for es igual así que no lo repito en otra función).//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const contagiadosDiaSiguiente3 = (grupoPacientes) =>{
    //Si el parametro recibido es null lanzamos error
    if (grupoPacientes == null) throw new Error("Grupo incorrecto 3");
    //Hacemos copia del grupo real para no alterar
    const grupoPacientesDiaSiguiente = [...grupoPacientes];

     // Recorremos el grupo(array) real para buscar a los sanos.
    grupoPacientes.forEach((paciente, indice) => {
        //Comprobamos si el paciente está sano (false)
        if (paciente === false){
           //Comprobamos si es el primer paciente y si el paciente a su "derecha" está contagiado y se contagia
            if (indice === 0 && grupoPacientes[indice+1] === true) grupoPacientesDiaSiguiente[indice] = true;

            //Si no es el primer paciente comprobamos si alguno de su "izquierda" o "derecha" está contagiado y se contagia
            else if (grupoPacientes[indice-1] === true || grupoPacientes[indice+1] === true) grupoPacientesDiaSiguiente[indice] = true;
        }
    });

    //Devuelvo el grupo(array) contagiado al siguiente día
    return grupoPacientesDiaSiguiente;
}


///////////////////////////////////////////////////
// Solución 4: buscamos SANOS(false) con un Map.//
//////////////////////////////////////////////////

const contagiadosDiaSiguiente4 = (grupoPacientes) =>{
    //Si el parametro recibido es null lanzamos error
    if (grupoPacientes == null) throw new Error("Grupo incorrecto 4");

     // Recorremos y mapeamos el grupo(array) real para buscar a los sanos.
    return grupoPacientes.map((paciente, indice) => {
        //Comprobamos si el paciente está sano (false)
        if (paciente === false){
           //Comprobamos si es el primer paciente y si el paciente a su "derecha" está contagiado entonces se contagia
            if (indice === 0 && grupoPacientes[indice+1] === true) return true;

            //Si no es el primer paciente comprobamos si alguno de su "izquierda" o "derecha" está contagiado entonces se contagia
            else if (grupoPacientes[indice-1] === true || grupoPacientes[indice+1] === true) return true;

            else return false;
        }
        return true;
    });
}
    


try {
    console.log("*******************CONTAGIADOS FOR*******************");
    console.log(contagiadosDiaSiguiente(grupoPacientes));
    console.log("***************CONTAGIADOS FOREACH***********************");
    console.log(contagiadosDiaSiguiente2(grupoPacientes));
    console.log("****************SANOS FOREACH**********************");
    console.log(contagiadosDiaSiguiente3(grupoPacientes));
    console.log("****************SANOS MAP**********************");
    console.log(contagiadosDiaSiguiente4(grupoPacientes));
    
} catch (error) {
    console.error(error);
    
}